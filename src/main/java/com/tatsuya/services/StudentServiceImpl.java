package com.tatsuya.services;

import com.tatsuya.dao.StudentDAO;
import com.tatsuya.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements IStudentService {

    @Autowired
    private StudentDAO studentDAO;

    @Override
    public List<Student> getAllStudents() {
        return studentDAO.findAll();
    }

    @Override
    public Student findStudent(String id) {
        return studentDAO.find(id);
    }
}
