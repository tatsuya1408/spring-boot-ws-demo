package com.tatsuya.services;

import com.tatsuya.models.Student;

import java.util.List;

public interface IStudentService {
    public abstract List<Student> getAllStudents();

    public abstract Student findStudent(String id);
}
