package com.tatsuya.controller;

import com.tatsuya.models.Student;
import com.tatsuya.services.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController extends BaseController {

    @Autowired
    private IStudentService iStudentService;

    /**
     * using ResponseEntity library to parse result to Json Objects
     */
    @GetMapping("/getAll")
    public ResponseEntity<List<Student>> getAll(HttpServletRequest request
    ) {
        List<Student> studentList = this.iStudentService.getAllStudents();
        return new ResponseEntity<List<Student>>(studentList, HttpStatus.OK);
    }

    @GetMapping("/find")
    public ResponseEntity<Student> findStudent(
            @RequestParam("id") String id,
            HttpServletRequest request
    ) {
        Student student = this.iStudentService.findStudent(id);
        return new ResponseEntity<Student>(student, HttpStatus.OK);
    }


    /**
     * another way:
     * using Gson library to parse result to Json Objects
     */
    @GetMapping("/getAllGSOn")
    public String getAllGSon(HttpServletRequest request
    ) {
        List<Student> studentList = this.iStudentService.getAllStudents();
        return super.gson.toJson(studentList);
    }

    @GetMapping("/findGSon")
    public String findStudentGSon(
            @RequestParam("id") String id,
            HttpServletRequest request
    ) {
        Student student = this.iStudentService.findStudent(id);
        return super.gson.toJson(student);
    }
}
