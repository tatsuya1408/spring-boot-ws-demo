package com.tatsuya.dao;

import com.tatsuya.models.Faculty;

import java.util.List;

public interface FacultyDAO {
    void create(Faculty faculty);

    void edit(Faculty faculty);

    void remove(Faculty faculty);

    Faculty find(Object id);

    List<Faculty> findAll();

    List<Faculty> findRange(int[] range);

    int count();
}
