package com.tatsuya.dao;

import com.tatsuya.models.Faculty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
@Repository
public class FacultyDAOImpl extends AbstractFacade<Faculty> implements FacultyDAO {
    @PersistenceContext
    private EntityManager entityManager;


    public FacultyDAOImpl() {
        super(Faculty.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
