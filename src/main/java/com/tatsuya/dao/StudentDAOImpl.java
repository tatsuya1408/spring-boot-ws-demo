package com.tatsuya.dao;

import com.tatsuya.models.Student;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
@Repository
public class StudentDAOImpl extends AbstractFacade<Student> implements StudentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public StudentDAOImpl() {
        super(Student.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
