package com.tatsuya.dao;

import com.tatsuya.models.Student;

import java.util.List;

public interface StudentDAO {
    void create(Student student);

    void edit(Student student);

    void remove(Student student);

    Student find(Object id);

    List<Student> findAll();

    List<Student> findRange(int[] range);

    int count();
}
