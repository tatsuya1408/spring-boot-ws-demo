package com.tatsuya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootWsDemoApplication.class, args);
    }
}
