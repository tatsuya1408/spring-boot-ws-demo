package com.tatsuya.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "faculty")
@NamedQueries({
        @NamedQuery(name = "Faculty.findAll", query = "SELECT f FROM Faculty f")
        , @NamedQuery(name = "Faculty.findByFacultyId", query = "SELECT f FROM Faculty f WHERE f.facultyId = :facultyId")
        , @NamedQuery(name = "Faculty.findByFacultyName", query = "SELECT f FROM Faculty f WHERE f.facultyName = :facultyName")})
public class Faculty {
    private String facultyId;
    private String facultyName;
    private List<Student> studentsByFacultyId;

    @Id
    @Column(name = "faculty_id")
    public String getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(String facultyId) {
        this.facultyId = facultyId;
    }

    @Basic
    @Column(name = "faculty_name")
    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Faculty faculty = (Faculty) o;
        return Objects.equals(facultyId, faculty.facultyId) &&
                Objects.equals(facultyName, faculty.facultyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(facultyId, facultyName);
    }

    @JsonBackReference
    @OneToMany(mappedBy = "facultyByFacultyId")
    public List<Student> getStudentsByFacultyId() {
        return studentsByFacultyId;
    }

    public void setStudentsByFacultyId(List<Student> studentsByFacultyId) {
        this.studentsByFacultyId = studentsByFacultyId;
    }
}
