﻿làm theo các bước sau:
1. tạo CSDL: mở file db.sql, copy nội dung & paste vào MySQL workbench hoặc qua MySQL Shell

2. sửa thông tin cấu hình CSDL trong file application.properties nằm tại đường dẫn src/main/resource
(sửa username & password của CSDL)

3. mở project = IntelliJ IDEA & run

4. truy cập 2 đường dẫn sau để test dịch vụ web:
127.0.0.1:8080/student/getAll
127.0.0.1:8080/student/find?id=B14DCCN096